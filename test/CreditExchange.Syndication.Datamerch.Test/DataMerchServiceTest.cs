﻿using Moq;
using System;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Datamerch.Proxy;

namespace LendFoundry.Syndication.Datamerch.Test
{
    public class DataMerchServiceTest
    {
        private Mock<IEventHubClient> EventHub { get; set; }
        private Mock<ILookupService> Lookup { get; set; }
        private IDatamerchProxy Proxy { get; set; }
        private SearchMerchantResponse dataMerchResponse { get; set; }
        private DataMerchService dataMerchService { get; }
        private IDatamerchConfiguration dataMerchConfiguration { get; set; }

        public DataMerchServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new DatamerchProxy(new DatamerchConfiguration
            {
                //simulation Configuration
               
                  AddMerchantsApiUrl   = "/datamerch/",
                  AuthorizationToken   = "WDs-s-NjbN4fqeNzYxiJ",
                  AddNoteApiUrl        = "/datamerch/",
                  SearchMerchantsApiUrl= "/datamerch/",
                  DatamerchBaseUrl     = "http://192.168.1.72:7022",
                  AuthorizationKey     = "zu4F9YcJaw",
                  FeinLength           = 7
                //actual configuration

                //AddMerchantsApiUrl = "/api/v1/merchants",
                //AuthorizationToken = "WDs-s-NjbN4fqeNzYxiJ",
                //AddNoteApiUrl = "/api/v1/notes",
                //SearchMerchantsApiUrl = "/api/v1/merchants",
                //DatamerchBaseUrl = "https://www.datamerch.com",
                //AuthorizationKey = "zu4F9YcJaw"
                //FeinLength = 7
            });
            dataMerchConfiguration = new DatamerchConfiguration
            {
                //simulation Configuration
                AddMerchantsApiUrl = "/datamerch/",
                AuthorizationToken = "WDs-s-NjbN4fqeNzYxiJ",
                AddNoteApiUrl = "/datamerch/",
                SearchMerchantsApiUrl = "/datamerch/",
                DatamerchBaseUrl = "http://192.168.1.67:7022",
                AuthorizationKey = "zu4F9YcJaw",
                FeinLength = 7
                //actual configuration

                //AddMerchantsApiUrl = "/api/v1/merchants",
                //AuthorizationToken = "WDs-s-NjbN4fqeNzYxiJ",
                //AddNoteApiUrl = "/api/v1/notes",
                //SearchMerchantsApiUrl = "/api/v1/merchants",
                //DatamerchBaseUrl = "https://www.datamerch.com",
                //AuthorizationKey = "zu4F9YcJaw"
                //FeinLength = 7
            };
            dataMerchResponse = new SearchMerchantResponse();
            dataMerchService = new DataMerchService(Proxy, EventHub.Object, Lookup.Object,dataMerchConfiguration);
        }

        [Fact]
        public void ArgumentNullExceptionWithMissingParameteresSearchMerchant()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("", "Test", "1234567"));
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("application", "", "1234567"));
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("application", "Test", ""));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => dataMerchService.SearchMerchant("Test", "Test", "12345"));
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("application", "Test", ""));
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("", "Test", "1234567"));
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant("application", "", "1234567"));
        }

        /// <summary>
        ///all  parameters null
        /// </summary>
        [Fact]
        public void VerifyThrowsArgumentExceptionWithNull_SearchMerchant()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => dataMerchService.SearchMerchant(null, null, null));
        }

        /// <summary>
        /// Success response with notnull
        /// </summary>
        [Fact]
        public void VerifySuccessWithNotNull_SearchOrganization()
        {
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var SearchMerchantResponse = dataMerchService.SearchMerchant("application", "CA000001", "7874633").Result;
            Assert.NotNull(SearchMerchantResponse.Merchants);
        }

        [Fact]
        public void VerifySuccessWithNoContentException_SearchOrganization()
        {
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => dataMerchService.SearchMerchant("application", "CA000001", "5555555"));
        }

        [Fact]
        public void VerifyInValidfeinID_SearchMerchant()
        {
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => dataMerchService.SearchMerchant("application", "CA000001", "123"));
        }
    }
}