﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.Datamerch.Proxy;
using Xunit;

namespace LendFoundry.Syndication.Datamerch.Test
{
    public class DatamerchConfigurationTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new DatamerchProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var dataMerchConfiguration = new DatamerchConfiguration
            {
                AddMerchantsApiUrl = "",
                AddNoteApiUrl = "",
                AuthorizationKey = "",
                AuthorizationToken = "",
                DatamerchBaseUrl = "",
                SearchMerchantsApiUrl = ""
            };

            Assert.Throws<ArgumentNullException>(() => new DatamerchProxy(dataMerchConfiguration));

            var dataMerchConfiguration1 = new DatamerchConfiguration
            {
                AddMerchantsApiUrl = "www.sigma.com",
                AddNoteApiUrl = "www.test.com",
                AuthorizationKey = "1234",
                AuthorizationToken = "",
                DatamerchBaseUrl = "",
                SearchMerchantsApiUrl = ""
            };

            Assert.Throws<ArgumentNullException>(() => new DatamerchProxy(dataMerchConfiguration1));
            var dataMerchConfiguration2 = new DatamerchConfiguration
            {
                AddMerchantsApiUrl = "www.sigma.com",
                AddNoteApiUrl = "www.test.com",
                AuthorizationKey = "1234",
                AuthorizationToken = "",
                DatamerchBaseUrl = "3456",
                SearchMerchantsApiUrl = "www.api.com"
            };

            Assert.Throws<ArgumentNullException>(() => new DatamerchProxy(dataMerchConfiguration2));
        }
    }
}