﻿using LendFoundry.Syndication.Datamerch.Client;
using LendFoundry.Syndication.Datamerch.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Datamerch.Client.Test
{
    public class DatamerchServiceClientTest
    {
        private Mock<IServiceClient> Client { get; set; }
        public DatamerchService dataMerchServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        public DatamerchServiceClientTest()
        {
            Client = new Mock<IServiceClient>();
            dataMerchServiceClient = new DatamerchService(Client.Object);
        }

        [Fact]
        public async void client_SearchMerchant()
        {
            Syndication.Datamerch.Proxy.SearchMerchantResponse dataMerchResponse = new Syndication.Datamerch.Proxy.SearchMerchantResponse();
            dataMerchResponse.Merchants = null;

            Client.Setup(s => s.ExecuteAsync<SearchMerchantResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new SearchMerchantResponse(dataMerchResponse));

            var response = await dataMerchServiceClient.SearchMerchant("application", "test", It.IsAny<string>());
            Assert.Equal("/{entitytype}/{entityid}/datamerch/search_merchant/{fein}", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }
    }
}