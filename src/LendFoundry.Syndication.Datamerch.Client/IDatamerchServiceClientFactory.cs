﻿using LendFoundry.Syndication.Datamerch;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Datamerch.Client
{
    /// <summary>
    /// Interface IDatamerchServiceClientFactory
    /// </summary>
    public interface IDatamerchServiceClientFactory
    {
        #region Public Methods

        IDataMerchService Create(ITokenReader reader);

        #endregion Public Methods
    }
}