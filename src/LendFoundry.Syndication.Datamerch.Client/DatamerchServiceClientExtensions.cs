﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Datamerch.Client
{
    public static class DatamerchServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        /// Adds the lenddo service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns>IServiceCollection.</returns>
        public static IServiceCollection AddDatamerchService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDatamerchServiceClientFactory>(p => new DatamerchServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDatamerchServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDatamerchService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IDatamerchServiceClientFactory>(p => new DatamerchServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IDatamerchServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDatamerchService(this IServiceCollection services)
        {
            services.AddSingleton<IDatamerchServiceClientFactory>(p => new DatamerchServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IDatamerchServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}
