﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;
using LendFoundry.Syndication.Datamerch;
using LendFoundry.Syndication.Datamerch.Response;

/// <summary>
/// The Client namespace.
/// </summary>
namespace LendFoundry.Syndication.Datamerch.Client
{
    public class DatamerchService : IDataMerchService
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DatamerchService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public DatamerchService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>The client.</value>
        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Searches the merchant.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="fein">The fein.</param>
        /// <returns>Task&lt;ISearchMerchantResponse&gt;.</returns>
        public async Task<ISearchMerchantResponse> SearchMerchant(string entityType, string entityId, string fein)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/datamerch/search_merchant/{fein}", Method.GET);
            request.AddUrlSegment("fein", fein);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<SearchMerchantResponse>(request);
        }

        #endregion Public Methods
    }
}