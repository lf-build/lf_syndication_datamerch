﻿using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Datamerch;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Datamerch.Client
{
    public class DatamerchServiceClientFactory : IDatamerchServiceClientFactory
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DatamerchServiceClientFactory"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        public DatamerchServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", Endpoint, Port).Uri;

        }

        public DatamerchServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the endpoint.
        /// </summary>
        /// <value>The endpoint.</value>
        private string Endpoint { get; }

        /// <summary>
        /// Gets the port.
        /// </summary>
        /// <value>The port.</value>
        private int Port { get; }

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        private IServiceProvider Provider { get; }

        private Uri Uri { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>IDataMerchService.</returns>
        public IDataMerchService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("datamerch");
            }

            var client = Provider.GetServiceClient(reader, uri);
            return new DatamerchService(client);
        }

        #endregion Public Methods
    }
}