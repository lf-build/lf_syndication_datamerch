﻿using LendFoundry.Syndication.Datamerch;
using LendFoundry.Syndication.Datamerch.Proxy;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Datamerch.Api.Controller
{
    /// <summary>
    /// ApiController
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the ApiController
        /// </summary>
        /// <param name="service">The service.</param>
        public ApiController(IDataMerchService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <value>The service.</value>
        private IDataMerchService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Searches the merchant.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="fein">Merchant Id fein.</param>
        /// <returns></returns>
        [HttpGet("/{entitytype}/{entityid}/datamerch/search_merchant/{fein}")]
        [ProducesResponseType(typeof(ISearchMerchantResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SearchMerchant(string entityType, string entityId, string fein)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.SearchMerchant(entityType, entityId, fein)));
                }
                catch (DatamerchException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        #endregion Public Methods
    }
}