﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Response
{
    public interface ISearchMerchantResponse
    {
        IList<IMerchantResponse> Merchants { get; set; }
    }
}
