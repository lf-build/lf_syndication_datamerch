﻿namespace LendFoundry.Syndication.Datamerch.Response
{
    public interface INote
    {
        INoteData MerchantNote { get; set; }
    }
}
