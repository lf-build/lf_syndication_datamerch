﻿namespace LendFoundry.Syndication.Datamerch.Response
{
    public interface IMerchantResponse
    {
        IMerchant Merchant { get; set; }
    }
}
