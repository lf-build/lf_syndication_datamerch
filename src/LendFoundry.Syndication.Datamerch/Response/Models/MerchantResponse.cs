﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Response
{
    public class MerchantResponse : IMerchantResponse
    {
        public MerchantResponse() { }
        public MerchantResponse(Proxy.IMerchantResponse merchant)
        {
            if (merchant != null)
                Merchant = new Merchant(merchant.Merchant);
        }
        [JsonConverter(typeof(InterfaceConverter<IMerchant, Merchant>))]
        public IMerchant Merchant { get; set; }
    }
}
