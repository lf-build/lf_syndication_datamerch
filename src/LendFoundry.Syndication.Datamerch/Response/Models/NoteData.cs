﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Response
{
    public class NoteData : INoteData
    {
        public NoteData() { }
        public NoteData(Proxy.INoteData noteData)
        {
            if (noteData != null)
            {
                Category = noteData.Category;
                Note = noteData.Note;
                CreatedAt = noteData.CreatedAt;
                AddedBy = noteData.AddedBy;
            }
        }
        public string Category { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string AddedBy { get; set; }

    }
}
