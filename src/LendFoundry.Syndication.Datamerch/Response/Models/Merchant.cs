﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Response
{

    public class Merchant : IMerchant
    {
        public Merchant() { }
        public Merchant(Proxy.IMerchant merchant)
        {
            if (merchant != null)
            {
                Fein = merchant.Fein;
                LegalName = merchant.LegalName;
                Dba = merchant.Dba;
                Address = merchant.Address;
                Street1 = merchant.Street1;
                Street2 = merchant.Street2;
                City = merchant.City;
                State = merchant.State;
                BusinessPhone = merchant.BusinessPhone;
                BusinessStartdate = merchant.BusinessStartdate;
                Industry = merchant.Industry;
                if (merchant.MerchantNotes != null)
                    CopyToMerchantNotes(merchant.MerchantNotes);
            }
        }
        public string Fein { get; set; }
        public string LegalName { get; set; }
        public string Dba { get; set; }
        public string Address { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessStartdate { get; set; }
        public string Industry { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<INote, Note>))]
        public IList<INote> MerchantNotes { get; set; }
        private void CopyToMerchantNotes(IList<Proxy.INote> noteList)
        {
            MerchantNotes = new List<INote>();
            foreach (var item in noteList)
            {
                MerchantNotes.Add(new Note(item));
            }
        }
    }
}
