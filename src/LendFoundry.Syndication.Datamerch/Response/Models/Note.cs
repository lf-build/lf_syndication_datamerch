﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Response
{
    public class Note : INote
    {
        public Note() { }
        public Note(Proxy.INote note)
        {
            if (note != null)
                MerchantNote = new NoteData(note.MerchantNote);
        }
        [JsonConverter(typeof(InterfaceConverter<INoteData, NoteData>))]
        public INoteData MerchantNote { get; set; }
    }

}
