﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Response
{
    public class SearchMerchantResponse : ISearchMerchantResponse
    {
        public SearchMerchantResponse()
        {
        }
        public SearchMerchantResponse(Proxy.ISearchMerchantResponse searchMerchantResponse)
        {
            if (searchMerchantResponse != null && searchMerchantResponse.Merchants != null)
                CopyToMerchant(searchMerchantResponse.Merchants);
        }
        [JsonConverter(typeof(InterfaceListConverter<IMerchantResponse, MerchantResponse>))]
        public IList<IMerchantResponse> Merchants { get; set; }
        private void CopyToMerchant(IList<Proxy.IMerchantResponse> merchantList)
        {
            Merchants = new List<IMerchantResponse>();
            foreach (var item in merchantList)
            {
                Merchants.Add(new MerchantResponse(item));
            }
        }

    }
}
