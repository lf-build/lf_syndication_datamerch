﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Datamerch.Events
{
    public class DatamerchSearchMerchantRequesteFail : SyndicationCalledEvent
    {
    }
}
