﻿using LendFoundry.Syndication.Datamerch.Events;
using LendFoundry.Syndication.Datamerch.Proxy;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using System;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// The Datamerch namespace.
/// </summary>
namespace LendFoundry.Syndication.Datamerch
{
    /// <summary>
    /// Class DataMerchService.
    /// </summary>
    /// <seealso cref="LendFoundry.Syndication.Datamerch.IDataMerchService" />
    public class DataMerchService : IDataMerchService
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMerchService" /> class.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="eventHub">The event hub.</param>
        /// <param name="lookup">The lookup.</param>
        /// <exception cref="System.ArgumentNullException">proxy
        /// or
        /// eventHub
        /// or
        /// lookup</exception>
        public DataMerchService(IDatamerchProxy proxy, IEventHubClient eventHub, ILookupService lookup, IDatamerchConfiguration configuration,ILogger logger,ITenantTime tenantTime)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Configuration = configuration;
            Logger = logger;
            TenantTime = tenantTime;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the event hub.
        /// </summary>
        /// <value>The event hub.</value>
        private IEventHubClient EventHub { get; }

        /// <summary>
        /// Gets the lookup.
        /// </summary>
        /// <value>The lookup.</value>
        private ILookupService Lookup { get; }

        private IDatamerchConfiguration Configuration { get; }

        /// <summary>
        /// Gets the proxy.
        /// </summary>
        /// <value>The proxy.</value>
        private IDatamerchProxy Proxy { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }


        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Ensures the type of the entity.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="ArgumentNullException">Invalid Entity Type
        /// or
        /// Invalid Entity Type</exception>
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new ArgumentNullException("Invalid Entity Type");

            return entityType;
        }

        /// <summary>
        /// Searches the merchant.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="fein">The fein.</param>
        /// <returns>Task&lt;Response.ISearchMerchantResponse&gt;.</returns>
        /// <exception cref="System.ArgumentNullException">fein</exception>
        /// <exception cref="System.ArgumentException">fein</exception>
        public async Task<Response.ISearchMerchantResponse> SearchMerchant(string entityType, string entityId, string fein)
        {
            entityType = EnsureEntityType(entityType);
            Logger.Info("Started Execution for SearchMerchant Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: DataMerch");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrEmpty(fein))
                throw new ArgumentException($"Please provide {nameof(fein)} Number");

            if (fein.Length != Configuration.FeinLength)
                throw new ArgumentException($"{nameof(fein)} cannot more than or less than {Configuration.FeinLength} digit.Please provide last {Configuration.FeinLength} digit only");
            try
            {
                var response = Proxy.SearchMerchant(fein);
                var result = new Response.SearchMerchantResponse(response);
                await EventHub.Publish(new DatamerchSearchMerchantRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = fein,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
            });
                Logger.Info("Completed Execution for SearchMerchant Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: DataMerch");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SearchMerchant Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Syndication: DataMerch" + "Exception" + exception.Message);

                await EventHub.Publish(new DatamerchSearchMerchantRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = fein,
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        #endregion Public Methods
    }
}