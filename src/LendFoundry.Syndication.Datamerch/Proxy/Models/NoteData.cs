﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public class NoteData: INoteData
    {
        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string CreatedAt { get; set; }
        [JsonProperty(PropertyName = "added_by")]
        public string AddedBy { get; set; }
        
    }
}
