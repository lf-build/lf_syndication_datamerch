﻿namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface INote
    {
        INoteData MerchantNote { get; set; }
    }
}
