﻿namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface IMerchantResponse
    {
        IMerchant Merchant { get; set; }
    }
}
