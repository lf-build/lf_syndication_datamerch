﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface IMerchant
    {
        string Fein { get; set; }
        string LegalName { get; set; }
        string Dba { get; set; }
        string Address { get; set; }
        string Street1 { get; set; }
        string Street2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string BusinessPhone { get; set; }
        string BusinessStartdate { get; set; }
        string Industry { get; set; }
        IList<INote> MerchantNotes { get; set; }
    }
}
