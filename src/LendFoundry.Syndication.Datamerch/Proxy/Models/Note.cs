﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public class Note: INote
    {
        [JsonConverter(typeof(ConcreteJsonConverter<NoteData>))]
        [JsonProperty(PropertyName = "note")]
        public INoteData MerchantNote { get; set; }
    }
    
}
