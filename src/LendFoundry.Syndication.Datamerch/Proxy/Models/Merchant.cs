﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Proxy
{

    public class Merchant:IMerchant
    {
        [JsonProperty(PropertyName = "fein")]
        public string Fein { get; set; }
        [JsonProperty(PropertyName = "legal_name")]
        public string LegalName { get; set; }
        [JsonProperty(PropertyName = "dba")]
        public string Dba { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "street1")]
        public string Street1 { get; set; }
        [JsonProperty(PropertyName = "street2")]
        public string Street2 { get; set; }
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }
        [JsonProperty(PropertyName = "business_phone")]
        public string BusinessPhone { get; set; }
        [JsonProperty(PropertyName = "business_startdate")]
        public string BusinessStartdate { get; set; }
        [JsonProperty(PropertyName = "industry")]
        public string Industry { get; set; }
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Note>, INote>))]
        [JsonProperty(PropertyName = "notes")]
        public IList<INote> MerchantNotes { get; set; }
    }
}
