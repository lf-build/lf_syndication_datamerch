﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public class MerchantResponse: IMerchantResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<Merchant>))]
        [JsonProperty(PropertyName = "merchant")]
        public IMerchant Merchant { get; set; }
    }
}
