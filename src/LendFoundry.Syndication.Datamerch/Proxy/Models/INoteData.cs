﻿namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface INoteData
    {
        string Category { get; set; }
        string Note { get; set; }
        string CreatedAt { get; set; }
        string AddedBy { get; set; }
    }
}
