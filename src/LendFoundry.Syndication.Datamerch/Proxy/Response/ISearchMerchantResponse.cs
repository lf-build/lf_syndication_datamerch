﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface ISearchMerchantResponse
    {
        IList<IMerchantResponse> Merchants { get; set; }
    }
}
