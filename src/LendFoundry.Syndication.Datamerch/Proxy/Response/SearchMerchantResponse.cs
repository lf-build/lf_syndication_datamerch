﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public class SearchMerchantResponse : ISearchMerchantResponse
    {
        public SearchMerchantResponse()
        {
        }
        public SearchMerchantResponse(List<MerchantResponse> merchantList)
        {
            if (merchantList != null)
                CopyToMerchant(merchantList);
        }
        [JsonConverter(typeof(ConcreteJsonConverter<Merchant>))]
        [JsonProperty(PropertyName = "merchant")]
        public IList<IMerchantResponse> Merchants { get; set; }
        private void CopyToMerchant(List<MerchantResponse> merchantList)
        {
            Merchants = new List<IMerchantResponse>();
            foreach (var item in merchantList)
            {
                Merchants.Add(item);
            }
        }
    }
}
