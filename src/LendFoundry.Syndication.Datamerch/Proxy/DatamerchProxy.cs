﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public class DatamerchProxy : IDatamerchProxy
    {
        public DatamerchProxy(IDatamerchConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.AuthorizationToken))
                throw new ArgumentNullException(nameof(configuration.AuthorizationToken));
            if (string.IsNullOrWhiteSpace(configuration.AuthorizationKey))
                throw new ArgumentNullException(nameof(configuration.AuthorizationKey));
            if (string.IsNullOrWhiteSpace(configuration.DatamerchBaseUrl))
                throw new ArgumentNullException(nameof(configuration.DatamerchBaseUrl));
            if (string.IsNullOrWhiteSpace(configuration.SearchMerchantsApiUrl))
                throw new ArgumentNullException(nameof(configuration.SearchMerchantsApiUrl));
            if (string.IsNullOrWhiteSpace(configuration.AddMerchantsApiUrl))
                throw new ArgumentNullException(nameof(configuration.AddMerchantsApiUrl));
            if (string.IsNullOrWhiteSpace(configuration.AddNoteApiUrl))
                throw new ArgumentNullException(nameof(configuration.AddNoteApiUrl));
            Configuration = configuration;
        }

        private IDatamerchConfiguration Configuration { get; }

        public ISearchMerchantResponse SearchMerchant(string fein)
        {
            if (string.IsNullOrWhiteSpace(fein))
                throw new ArgumentNullException(nameof(fein));
            if (fein.Length != Configuration.FeinLength)
                throw new ArgumentException($"{nameof(fein)} cannot more than or less than {Configuration.FeinLength} digit.Please provide last {Configuration.FeinLength} digit only");
            var client = new RestClient(Configuration.DatamerchBaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest($"{Configuration.SearchMerchantsApiUrl.TrimEnd('/')}/{fein}", Method.GET);
            var response = ExecuteRequest<List<MerchantResponse>>(client, request);
            return new SearchMerchantResponse(response);
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            client.Authenticator = new HttpBasicAuthenticator(Configuration.AuthorizationToken, Configuration.AuthorizationKey);
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new DatamerchException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new DatamerchException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.ErrorMessage ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new DatamerchException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new DatamerchException(response.ErrorMessage);

            if (response.StatusCode == HttpStatusCode.NoContent)
                throw new DatamerchException(response.ErrorMessage);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new DatamerchException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.ErrorMessage ?? ""}");

            if (response.StatusCode != HttpStatusCode.OK)
                throw new DatamerchException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.ErrorMessage ?? ""}");

            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.ErrorMessage, exception);
            }
        }
    }
}