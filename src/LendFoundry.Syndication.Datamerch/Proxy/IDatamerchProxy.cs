﻿using LendFoundry.Syndication.Datamerch.Response;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Datamerch.Proxy
{
    public interface IDatamerchProxy
    {
        ISearchMerchantResponse SearchMerchant(string fein);
    }
}
