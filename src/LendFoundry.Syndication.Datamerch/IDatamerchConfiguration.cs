﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.Datamerch
{
    /// <summary>
    /// Interface IDatamerchConfiguration
    /// </summary>
    public interface IDatamerchConfiguration : IDependencyConfiguration
    {
        #region Public Properties

        string AddMerchantsApiUrl { get; set; }
        string AddNoteApiUrl { get; set; }
        string AuthorizationKey { get; set; }
        string AuthorizationToken { get; set; }
        string DatamerchBaseUrl { get; set; }
        string SearchMerchantsApiUrl { get; set; }
        int FeinLength { get; set; }
        string ConnectionString { get; set; }

        #endregion Public Properties
    }
}