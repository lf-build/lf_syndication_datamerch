﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.Datamerch
{
    /// <summary>
    /// Class DatamerchConfiguration.
    /// </summary>
    /// <seealso cref="LendFoundry.Syndication.Datamerch.IDatamerchConfiguration" />
    public class DatamerchConfiguration : IDatamerchConfiguration, IDependencyConfiguration
    {
        #region Public Properties

        public string AddMerchantsApiUrl { get; set; }
        public string AddNoteApiUrl { get; set; }
        public string AuthorizationKey { get; set; }
        public string AuthorizationToken { get; set; }
        public string DatamerchBaseUrl { get; set; }
        public string SearchMerchantsApiUrl { get; set; }

        public int FeinLength { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

        #endregion Public Properties
    }
}