﻿using LendFoundry.Syndication.Datamerch.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Datamerch
{
    /// <summary>
    /// Interface IDataMerchService
    /// </summary>
    public interface IDataMerchService
    {
        #region Public Methods

        Task<ISearchMerchantResponse> SearchMerchant(string entityType, string entityId, string fein);

        #endregion Public Methods
    }
}